package checkers;

import checkers.gui.SlotFactory;
import checkers.gui.Window;
import checkers.server.ChessFacade;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {


    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Pane pane = new Pane();
        ChessFacade chessFacade = new ChessFacade();
        Scene scene = new Scene(pane);
        SlotFactory slotFactory = new SlotFactory();
        primaryStage.setScene(scene);
        Window window = new Window(pane, scene, slotFactory,chessFacade);
        primaryStage.setResizable(false);
        primaryStage.show();

 // slot to jest miejsce na planszy o danych koordynatach, przechowuje referencje do pola i checkera ktory sie na nim znajduje

    }

}
//cała tablica CheckerView (z referencjami do Field)
//wstawianie i wyswietlanie checkerow w 2 kolorach
//-> łapanie inputu
