package checkers.gui;

import checkers.server.ChessFacade;
import checkers.server.FigureDto;

//reakcja na klikniecie -> wysokopoziomowa -> zbijamy, zaznaczamy figure, przemieszczamy figury
public class GameInteraction {

    private Slot selectedSlot = null;

    private ChessFacade chessFacade;

    public GameInteraction(ChessFacade chessFacade) {
        this.chessFacade = chessFacade;
    }

    public void reactToAction(Slot newSlot) {
        //wykonujemy akcje -> zaznaczenie figury/przesuniecie/atak
        if (hasFigure(newSlot) && newSlot.getFigure().getFigureColor() == chessFacade.currentGamer()) {
            select(newSlot);
        } else if (selected() && !hasFigure(newSlot)) {
            move(newSlot);

        } else if (selected()) {
            attack(newSlot);
        }
        //klikam własną/klikam puste pole/ klikam przeciwnika
        // zaznaczam /ruszam się/ atakuję

    }

    private boolean hasFigure(Slot newSlot) {
        return newSlot.getFigure() != null;
    }

    private boolean selected() {
        return selectedSlot != null;
    }

    void select(Slot slot) {
        if (selected()) {
            //jeśli klikasz kolejną figurę zgaś podswietlenie z poprzedniej
            selectedSlot.getField().restoreBackgroundColor(); // gasi stare pole
        }
        changeColor(slot); // podswietlamy nowe pole
        selectedSlot = slot; // zapamietuje slota zeby go potem zgasic

    }

    void move(Slot slot) {
        FigureDto notMovedFigureDTO = selectedSlot.getFigure();
        if (!chessFacade.canMove(notMovedFigureDTO, slot.getColumn(), slot.getRow())) {
            System.out.println("niepoprawny ruch");
            return;
        }
        FigureDto movedFigureDto = chessFacade.moveFigure(notMovedFigureDTO, slot.getColumn(), slot.getRow());
        moveToCorrectSlot(slot, movedFigureDto);

    }

    void attack(Slot slot) {
        FigureDto notMovedFigureDTO = selectedSlot.getFigure();
        if (!chessFacade.canMove(notMovedFigureDTO, slot.getColumn(), slot.getRow())) {
            System.out.println("niepoprawny ruch");
            return;
        }
        slot.cleanSlot();
        FigureDto movedFigureDto = chessFacade.attackFigure(notMovedFigureDTO, slot.getColumn(), slot.getRow());
        moveToCorrectSlot(slot, movedFigureDto);
    }

    private void moveToCorrectSlot(Slot slot, FigureDto movedFigureDto) {
        slot.setFigure(movedFigureDto);
        selectedSlot.cleanSlot();
        selectedSlot.getField().restoreBackgroundColor();
        selectedSlot = null; // wykonalismy ruch wiec nie ma juz zaznaczonego slota, trzeba zaznaczyc jeszcze raz
    }

    private void changeColor(Slot slot) {
        Field field = slot.getField();
        field.changeBackgroundColor(); // podswietlam
    }
    //kliknięto figurę -> musimy zapamietac jaką figure kliknieto
    //kliknieto puste pole -> przemisc zaznaczona figure w te miejsce

}
