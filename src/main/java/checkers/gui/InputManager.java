package checkers.gui;

import javafx.scene.input.MouseEvent;

//rekacja na klikniecie -> niskopoziomowa (sprawdzenie jakie pole zostalo klikniete itd)
public class InputManager {

    private Slot[][] slots;
    private GameInteraction gameInteraction;



    public InputManager(Slot[][] slots, GameInteraction gameInteraction) {
        this.slots = slots;
        this.gameInteraction = gameInteraction;
    }

    public void reactToClick(MouseEvent mouseEvent) {
        int x = (int) (mouseEvent.getX() / Field.SIZE);
        int y = (int) (mouseEvent.getY() / Field.SIZE);

        gameInteraction.reactToAction(slots[y][x]);
    }

}
