package checkers.gui;

import checkers.server.ChessFacade;
import checkers.server.FigureDto;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import java.util.Arrays;
import java.util.List;

public class Window {

    private SlotFactory slotFactory;
    private Pane pane;
    private Scene scene;
    private Slot[][] slots;
    private ChessFacade chessFacade;
    private GameInteraction gameInteraction;

    public Window(Pane pane, Scene scene, SlotFactory slotFactory, ChessFacade chessFacade) {
        this.pane = pane;
        this.scene = scene;
        this.slotFactory = slotFactory;
        this.chessFacade = chessFacade;
        this.prepareDependencies();
        this.initComponents();
        this.initInteractions();
        //   this.attachMouseInput();
    }

    public void prepareDependencies() {
        gameInteraction = new GameInteraction(chessFacade);
    }

    public void initComponents() {
        slots = slotFactory.createBord();
        //wstawiamy szachownice (podloga)
        for (Slot[] row : slots) {
            for (Slot slot : row) {
                pane.getChildren().add(slot.getField());
            }
        }
        //wstawiamy sloty (figury/miejsca na figury)
        for (Slot[] row : slots) {
            for (Slot slot : row) {
                pane.getChildren().add(slot);
            }
        }
        // wyciagamy z backendu figruy i wstawiamy je do slotów ( narzucamy na gotową już planszę figury )
        List<FigureDto> allFigures = chessFacade.getAllFigures();
        for (FigureDto figure : allFigures) {
            slots[figure.getY()][figure.getX()].setFigure(figure);
        }
        System.out.println(Arrays.deepToString(slots));
    }

    public void initInteractions() {
        scene.setOnMouseClicked(new InputManager(slots, gameInteraction)::reactToClick);
    }

    public void attachMouseInput() {
        Slot source = slots[7][0];
        Field target = slots[7][2].getField();
        //najazd na target
        //wyjscie z targeta
        source.setOnDragDetected(e -> {
            System.out.println("Detected!");
            Dragboard db = scene.startDragAndDrop(TransferMode.ANY); //przesyłanie danych od komponentu do komponentu drag and dropem -> transfer
            String data = "hello";
            ClipboardContent content = new ClipboardContent();
            content.putString(data);
            db.setContent(content);
            e.consume();
        });
        target.setOnDragOver(e -> {
            System.out.println("over");
            if (e.getGestureSource() != target &&
                    e.getDragboard().hasString()) {
                e.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            }
            e.consume();
        });
        target.setOnDragEntered(e -> {
            System.out.println("ENTER");
            if (e.getGestureSource() != target && e.getDragboard().hasString()) {
                target.setFill(Color.BLUE);
            }
            e.consume();
        });

        target.setOnDragExited(e -> {
            System.out.println("Drag Exited");
            target.setFill(Color.WHITE);
            e.consume();
        });

        target.setOnDragDropped(e -> {
            System.out.println("dropped");
            Dragboard db = e.getDragboard();
            boolean sucess = false;
            if (db.hasString()) {
                System.out.println(db.getString());
                target.setFill(Color.GREEN);
                sucess = true;
            }
            e.setDropCompleted(sucess);
            e.consume();
        });

     /*   source.setOnDragDone(e-> {
            System.out.println("Done" );
            if (e.getTransferMode() == TransferMode.MOVE) {
                source.getField().setFill(Color.ORANGE);
            }
            e.consume();
        });*/

        source.setOnDragDone(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                /* the drag-and-drop gesture ended */
                /*  System.out.println("onDragDone");
                 *//* if the data was successfully moved, clear it *//*
                if (event.getTransferMode() == TransferMode.MOVE) {
                    source.setText("");
                }

                event.consume();
*/
                System.out.println("Done");
                if (event.getTransferMode() == TransferMode.MOVE) {
                    source.getField().setFill(Color.ORANGE);
                }
                event.consume();
            }
        });
    }
}
