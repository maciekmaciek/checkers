package checkers.gui;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Field extends Rectangle { // Object

    protected static final int SIZE = 120;
    private int column;
    private int row;
    private Color baseColor;

    public Field(int column, int row, Color baseColor) {
        super(column * SIZE, row * SIZE, SIZE, SIZE);
        this.column = column;
        this.row = row;
        this.baseColor = baseColor;
        setFill(baseColor);
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    void changeBackgroundColor() {
        setFill(Color.BEIGE);
    }

    void restoreBackgroundColor() {
        setFill(baseColor);
    }

}
