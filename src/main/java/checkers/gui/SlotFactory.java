package checkers.gui;

import javafx.scene.paint.Color;

public class SlotFactory {
    // tworzymy plansze jako tablice 8 na 8
    public Slot[][] createBord() {
        Color actualColor = null;
        Slot[][] slots = new Slot[8][8];
        for (int y = 0; y < slots.length; y++) {
            for (int x = 0; x < slots.length; x++) {
                if ( ( x + y ) % 2 == 0) {
                    actualColor = Color.BLACK;
                } else {
                    actualColor = Color.WHITE;
                }
                slots[y][x] = new Slot(new Field(x, y, actualColor));
            }
        }
        return slots;
    }
}
