package checkers.gui;

import checkers.server.FigureColor;
import checkers.server.FigureDto;
import checkers.server.FigureType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Slot extends ImageView {

    private FigureDto figure;
    private Field field;

    public Slot(FigureDto figure) { // z figura
        this.figure = figure;
    }

    public Slot(Field field) { // bez figury
        this.field = field;
    }

    public Field getField() {
        return field;
    }



    public void setFigure(FigureDto figure) {
        FigureColor figureColor = figure.getFigureColor();
        FigureType figureType = figure.getFigureType();
        this.figure = figure;
        char figureColorCharAt = figureColor.name().charAt(0);
        String name = figureType.getFigureName();
        String url = "assets/" + name + figureColorCharAt + ".png";
        setImage(new Image(url, Field.SIZE, Field.SIZE, false, true));
        setX(figure.getX() * Field.SIZE);
        setY(figure.getY() * Field.SIZE);
    }

    public void cleanSlot(){
        this.figure = null;
        setImage(null);

    }

    public FigureDto getFigure() {
        return figure;
    }

    @Override
    public String toString() {
        return "Slot{" +
                "figure=" + figure +
                ", field=" + field +
                '}';
    }

    public int getColumn() {
        return field.getColumn();
    }

    public int getRow() {
        return field.getRow();
    }


}
