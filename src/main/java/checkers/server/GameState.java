package checkers.server;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

public class GameState {

    private FigureFactory figureFactory;
    private List<Figure> figures = new ArrayList<>();
    private FigureColor currentGamer;

    public GameState() {
        this.currentGamer = FigureColor.WHITE;
        this.figureFactory = new FigureFactory(this);
        figures.addAll(figureFactory.buildFigures());
    }

    public List<Figure> getFigures() {
        return this.figures;
    }

    public Optional<Figure> findFigure(int x, int y) {
        for (Figure figure : figures) {
            if (figure.getX() == x && figure.getY() == y) {
                return Optional.of(figure);
            }
        }
        return Optional.empty();
    }




    public FigureColor getCurrentGamer() {
        return currentGamer;
    }

    public void removeFigure(Figure figure) {
        figures.remove(figure);
    }

    public void changeGamer() {
        if (currentGamer == FigureColor.WHITE) {
            currentGamer = FigureColor.BLACK;
        } else {
            currentGamer = FigureColor.WHITE;
        }
    }

}
