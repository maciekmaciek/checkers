package checkers.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class Figure {
    private int x;
    private int y;
    private final FigureColor figureColor;
    private final FigureType figureType;
    private GameState gameState;

    public Figure(int x, int y, FigureColor figureColor, FigureType figureType, GameState gameState) {
        this.x = x;
        this.y = y;
        this.figureColor = figureColor;
        this.figureType = figureType;
        this.gameState = gameState;
    }

    public boolean canMoveTo(int x, int y) {
        List<Move> possibleMoves = getPossibleMoves();
        for (Move possibleMove : possibleMoves) {
            if (possibleMove.getX() == x && possibleMove.getY() == y) {
                return true;
            }
        }
        return false;
    }

    public List<Move> getPossibleMoves() {
        List<Move> moves;
        if (this.figureType.equals(FigureType.ROOK)) {
            moves = generateRookMoves();
        } else if (this.figureType.equals(FigureType.PAWN)) {
            moves = generatePawnMoves();
        } else if (this.figureType.equals(FigureType.KNIGHT)) {
            moves = generateKnightMoves();
        } else if (this.figureType.equals(FigureType.BISHOP)) {
            moves = generateBishopMoves();
        } else if (this.figureType.equals(FigureType.KING)) {
            moves = generateKingMoves();
        } else if (this.figureType.equals(FigureType.QUEEN)) {
            moves = generateQueenMoves();
        } else {
            return Collections.EMPTY_LIST;
        }
        return moves;
    }

    private List<Move> generatePawnMoves() {
        List<Move> moves = new ArrayList<>();
        if (this.figureColor == FigureColor.WHITE) {
            for (int i = 1; i <= 2; i++) {
                if (this.y != 6 && i == 2) { // jesli y nie jest 6 i drugie powtorzenie petli to wychodze
                    break;
                }
                moves.add(new Move(this.x, this.y - i));
            }
            Optional<Figure> enemyLeft = this.gameState.findFigure(x - 1, y - 1);
            Optional<Figure> enemyRight = this.gameState.findFigure(x + 1, y - 1);
            if (enemyLeft.isPresent() && enemyLeft.get().figureColor == FigureColor.BLACK) {
                moves.add(new Move(this.x - 1, this.y - 1));
            }
            if (enemyRight.isPresent() && enemyRight.get().figureColor == FigureColor.BLACK) {
                moves.add(new Move(this.x + 1, this.y - 1));
            }

        } else {
            for (int i = 1; i <= 2; i++) {
                if (this.y != 1 && i == 2) {
                    break;
                }
                moves.add(new Move(this.x, this.y + i));
            }
            Optional<Figure> enemyLeft = this.gameState.findFigure(x - 1, y + 1);
            Optional<Figure> enemyRight = this.gameState.findFigure(x + 1, y + 1);
            if (enemyLeft.isPresent() && enemyLeft.get().figureColor == FigureColor.WHITE) {
                moves.add(new Move(this.x - 1, this.y + 1));
            }
            if (enemyRight.isPresent() && enemyRight.get().figureColor == FigureColor.WHITE) {
                moves.add(new Move(this.x + 1, this.y + 1));
            }
        }
        return moves;
    }

    private List<Move> generateRookMoves() {
        List<Move> moves = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            moves.add(new Move(i, this.y));
            moves.add(new Move(this.x, i));
        }
        moves.remove(new Move(this.x, this.y));
        moves.remove(new Move(this.x, this.y));
        return moves;
    }

    private List<Move> generateKnightMoves() {
        List<Move> moves = new ArrayList<>();
        moves.add(new Move(x + 1, y + 2));
        moves.add(new Move(x - 1, y + 2));
        moves.add(new Move(x - 2, y + 1));
        moves.add(new Move(x - 2, y - 1));
        moves.add(new Move(x - 1, y - 2));
        moves.add(new Move(x + 1, y - 2));
        moves.add(new Move(x + 2, y - 1));
        moves.add(new Move(x - 2, y + 1));

        moves.removeIf(move -> move.getX() < 0 || move.getY() < 0 || move.getX() > 7 || move.getY() > 7);
        return moves;
    }

    private List<Move> generateBishopMoves() {
        List<Move> moves = new ArrayList<>();
        for (int i = 0; x - i >= 0 && y + i <= 7; i++) {
            moves.add(new Move(x - i, y + i));
        }
        for (int i = 0; x + i <= 7 && y + i <= 7; i++) {
            moves.add(new Move(x + i, y + i));
        }
        for (int i = 0; x - i >= 0 && y - i >= 0; i++) {
            moves.add(new Move(x - i, y - i));
        }
        for (int i = 0; x + i <= 7 && y - i >= 0; i++) {
            moves.add(new Move(x + i, y - i));
        }
        return moves;
    }

    private List<Move> generateKingMoves() {
        List<Move> moves = new ArrayList<>();
        moves.add(new Move(x - 1, y + 1));
        moves.add(new Move(x, y + 1));
        moves.add(new Move(x - 1, y));
        moves.add(new Move(x + 1, y));
        moves.add(new Move(x + 1, y - 1));
        moves.add(new Move(x + 1, y + 1));
        moves.add(new Move(x, y - 1));
        moves.add(new Move(x - 1, y - 1));

        moves.removeIf(move -> move.getX() < 0 || move.getY() < 0 || move.getX() > 7 || move.getY() > 7);
        return moves;
    }

    private List<Move> generateQueenMoves() {
        List<Move> moves = new ArrayList<>(generateBishopMoves());
        moves.addAll(generateRookMoves());
        return moves;
    }

    public FigureDto toDTO() {
        return new FigureDto(x, y, figureColor, figureType);
    }


    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public FigureColor getFigureColor() {
        return this.figureColor;
    }

    public FigureType getFigureType() {
        return this.figureType;
    }

    @Override
    public String toString() {
        return "Checker: " +
                "x: " + this.x +
                ", y" + this.y +
                '}';
    }
}
