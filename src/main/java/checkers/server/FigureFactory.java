package checkers.server;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FigureFactory {

    private GameState state;

    public FigureFactory(GameState state) {
        this.state = state;
    }

    public Collection<Figure> buildFigures() {
        List<Figure> createFigureList = new ArrayList<>();
        figureBlackSite(createFigureList);
        figureWhiteSite(createFigureList);

        return createFigureList;
    }

    private void figureWhiteSite(List<Figure> createFigureList) {
        int y = 7;
        for (int i = 0; i < 8; i++) {
            createFigureList.add(new Figure(i, 6, FigureColor.WHITE, FigureType.PAWN, state));
        }

        createFigureList.add(new Figure(5, y, FigureColor.WHITE, FigureType.BISHOP, state));
        createFigureList.add(new Figure(2, y, FigureColor.WHITE, FigureType.BISHOP, state));
        createFigureList.add(new Figure(3, y, FigureColor.WHITE, FigureType.KING, state));
        createFigureList.add(new Figure(4, y, FigureColor.WHITE, FigureType.QUEEN, state));
        createFigureList.add(new Figure(7, y, FigureColor.WHITE, FigureType.ROOK, state));
        createFigureList.add(new Figure(0, y, FigureColor.WHITE, FigureType.ROOK, state));
        createFigureList.add(new Figure(1, y, FigureColor.WHITE, FigureType.KNIGHT, state));
        createFigureList.add(new Figure(6, y, FigureColor.WHITE, FigureType.KNIGHT, state));
    }

    private void figureBlackSite(List<Figure> createFigureList) {
        int y = 0;
        for (int i = 0; i < 8; i++) {
            createFigureList.add(new Figure(i, 1, FigureColor.BLACK, FigureType.PAWN, state));
        }
        createFigureList.add(new Figure(2, y, FigureColor.BLACK, FigureType.BISHOP, state));
        createFigureList.add(new Figure(5, y, FigureColor.BLACK, FigureType.BISHOP, state));
        createFigureList.add(new Figure(3, y, FigureColor.BLACK, FigureType.KING, state));
        createFigureList.add(new Figure(4, y, FigureColor.BLACK, FigureType.QUEEN, state));
        createFigureList.add(new Figure(0, y, FigureColor.BLACK, FigureType.ROOK, state));
        createFigureList.add(new Figure(7, y, FigureColor.BLACK, FigureType.ROOK, state));
        createFigureList.add(new Figure(1, y, FigureColor.BLACK, FigureType.KNIGHT, state));
        createFigureList.add(new Figure(6, y, FigureColor.BLACK, FigureType.KNIGHT, state));
    }
}
