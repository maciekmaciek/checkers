package checkers.server;

public enum FigureType {
    PAWN("Pawn"), KING("King"), ROOK("Rook"), KNIGHT("Knight"), QUEEN("Queen"), BISHOP("Bishop");

    private String figureName;

    FigureType(String figureName) {
        this.figureName = figureName;
    }

    public String getFigureName() {
        return figureName;
    }
}
