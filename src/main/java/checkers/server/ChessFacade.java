package checkers.server;

import java.util.ArrayList;
import java.util.List;

// Controller
public class ChessFacade {

    private GameState state = new GameState();

    public List<FigureDto> getAllFigures() {
        List<Figure> figures = state.getFigures();
        List<FigureDto> figureDtos = new ArrayList<>();
        for (Figure figure : figures) {
            FigureDto figureDto = new FigureDto(figure.getX(), figure.getY(), figure.getFigureColor(), figure.getFigureType());
            figureDtos.add(figureDto);
        }
        return figureDtos;
    }

    public FigureDto moveFigure(FigureDto figureDto, int x, int y) {
        Figure figure = state.findFigure(figureDto.getX(), figureDto.getY()).get();
        figure.setX(x);
        figure.setY(y);
        state.changeGamer();
        return figure.toDTO();
    }

    public FigureDto attackFigure(FigureDto figureDto, int x, int y) {
        Figure enemy = state.findFigure(x, y).get();
        state.removeFigure(enemy);
        return moveFigure(figureDto, x, y);
    }

    public FigureColor currentGamer() {
        return state.getCurrentGamer();
    }

    public boolean canMove(FigureDto figureDto, int x, int y) {
        Figure figure = state.findFigure(figureDto.getX(), figureDto.getY()).get();
        return figure.canMoveTo(x, y);
    }

}
