package checkers.server;

public final class FigureDto {
    private final int x;
    private final int y;
    private final FigureColor figureColor;
    private final FigureType figureType;

    public FigureDto(int x, int y, FigureColor figureColor, FigureType figureType) {
        this.x = x;
        this.y = y;
        this.figureColor = figureColor;
        this.figureType = figureType;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public FigureColor getFigureColor() {
        return figureColor;
    }

    public FigureType getFigureType() {
        return figureType;
    }

    @Override
    public String toString() {
        return "FigureDto{" +
                "x=" + x +
                ", y=" + y +
                ", figureColor=" + figureColor +
                ", figureType=" + figureType +
                '}';
    }
}
