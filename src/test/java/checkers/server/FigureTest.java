package checkers.server;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

class FigureTest {

    @Test
    @DisplayName("Should return all white rook moves from started position")
    void shouldReturnAllROOKMoves() {

        // given
        Figure figure = new Figure(0, 1, FigureColor.BLACK, FigureType.ROOK,null);
        // when

        List<Move> possibleMoves = figure.getPossibleMoves();

        // then
        Assertions.assertEquals(14, possibleMoves.size());

        Assertions.assertTrue(possibleMoves.contains(new Move(0, 0)));
        Assertions.assertTrue(possibleMoves.contains(new Move(0, 3)));
        Assertions.assertTrue(possibleMoves.contains(new Move(1, 1)));
        Assertions.assertTrue(possibleMoves.contains(new Move(5, 1)));
        Assertions.assertFalse(possibleMoves.contains(new Move(1, 2)));
        Assertions.assertFalse(possibleMoves.contains(new Move(1, 3)));
        Assertions.assertFalse(possibleMoves.contains(new Move(0, 1)));
    }

    @Test
    @DisplayName("Should return all white pawn moves from started position")
    void shouldReturnAllWhitePawnMoves() {
        // given
        Figure figure = new Figure(0, 6, FigureColor.WHITE, FigureType.PAWN,null);
        // when
        List<Move> pawnPossibleMoves = figure.getPossibleMoves();
        // then

        Assertions.assertEquals(2, pawnPossibleMoves.size());

        Assertions.assertTrue(pawnPossibleMoves.contains(new Move(0, 5)));
        Assertions.assertTrue(pawnPossibleMoves.contains(new Move(0, 4)));
        Assertions.assertFalse(pawnPossibleMoves.contains(new Move(0, 3)));
        Assertions.assertFalse(pawnPossibleMoves.contains(new Move(0, 6)));
        Assertions.assertFalse(pawnPossibleMoves.contains(new Move(0, 7)));
        Assertions.assertFalse(pawnPossibleMoves.contains(new Move(0, 8)));
        Assertions.assertFalse(pawnPossibleMoves.contains(new Move(1, 6)));

    }

    @Test
    @DisplayName("Should return all white pawn moves from different started position")
    void shouldReturnWhitePawnMoves() {
        // given
        Figure figure = new Figure(0, 4, FigureColor.WHITE, FigureType.PAWN,null);
        // when
        List<Move> pawnPossibleMoves = figure.getPossibleMoves();
        // then
        Assertions.assertEquals(1, pawnPossibleMoves.size());

        Assertions.assertTrue(pawnPossibleMoves.contains(new Move(0, 3)));

    }

}
